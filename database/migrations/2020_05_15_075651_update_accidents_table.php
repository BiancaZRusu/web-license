<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAccidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accidents', function ($table) {
            $table->integer('hospital_id')->nullable()->unsigned();
            $table->text('ambulance_distance_km')->nullable();
            $table->text('ambulance_distance_traffic')->nullable();
            $table->text('hospital_distance_km')->nullable();
            $table->text('hospital_distance_traffic')->nullable();
            $table->foreign('hospital_id')->references('id')->on('hospitals');
            $table->integer('ambulance_id')->nullable()->unsigned();
            $table->foreign('ambulance_id')->references('id')->on('emergency_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accidents', function ($table) {
            $table->dropForeign(['hospital_id']);
            $table->dropForeign(['ambulance_id']);
            $table->dropColumn('hospital');
            $table->dropColumn('ambulance');
            $table->dropColumn('ambulance_distance_km');
            $table->dropColumn('ambulance_distance_traffic');
            $table->dropColumn('hospital_distance_km');
            $table->dropColumn('hospital_distance_traffic');
        });
    }
}
