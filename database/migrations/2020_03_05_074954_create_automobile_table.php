<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutomobileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automobile', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('emergency_vehicle_id')->unsigned();
            $table->foreign('emergency_vehicle_id')->references('id')->on('emergency_vehicles');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('automobile');
    }
}
