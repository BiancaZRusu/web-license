<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergencyLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('emergency_locations')) {
            Schema::create('emergency_locations', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('city');
                $table->decimal('longitude', 18, 12);
                $table->decimal('latitude', 18, 12);
                $table->integer('vehicle_type_id')->unsigned();
                $table->foreign('vehicle_type_id')->references('id')->on('vehicle_type');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_type', function ($table) {
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
        });
        Schema::table('emergency_locations', function ($table) {
            $table->dropForeign(['vehicle_type_id']);
        });
        Schema::dropIfExists('emergency_locations');
    }
}
