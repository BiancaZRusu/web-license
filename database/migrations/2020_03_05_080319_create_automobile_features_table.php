<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutomobileFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automobile_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('automobile_id')->unsigned();
            $table->foreign('automobile_id')->references('id')->on('automobile');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('automobile_features', function ($table) {
            $table->dropForeign(['automobile_id']);
        });
        Schema::dropIfExists('automobile_features');
    }
}
