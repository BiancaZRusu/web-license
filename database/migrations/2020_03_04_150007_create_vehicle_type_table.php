<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_type')) {
            Schema::create('vehicle_type', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });
        }
        if (Schema::hasTable('vehicle_type')) {
            Schema::table('emergency_vehicles', function ($table) {
                $table->foreign('vehicle_type_id')->references('id')->on('vehicle_type');
            });
            DB::table('vehicle_type')->insert(
                array(
                    array(
                        'name' => 'ambulance',
                        'created_at' => now(),
                        'updated_at' => now(),
                    ),
                    array(
                        'name' => 'smurd',
                        'created_at' => now(),
                        'updated_at' => now(),
                    )
                )
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emergency_vehicles', function ($table) {
            $table->dropForeign(['vehicle_type_id']);
        });
        Schema::dropIfExists('vehicle_type');
    }
}
