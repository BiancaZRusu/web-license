<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeOfVictimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('type_of_victims')) {
            Schema::create('type_of_victims', function (Blueprint $table) {
                $table->increments('id');
                $table->string('type');
                $table->timestamps();
            });
        }
        Schema::table('accidents', function ($table) {
            $table->foreign('type_of_victim')->references('id')->on('type_of_victims');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accidents', function ($table) {
            $table->dropForeign(['type_of_victim']);
        });
        Schema::dropIfExists('type_of_victims');
    }
}
