<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesMedicalPersonnelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles_medical_personnel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emergency_vehicle_id')->unsigned();
            $table->foreign('emergency_vehicle_id')->references('id')->on('emergency_vehicles');
            $table->integer('medical_personnel_id')->unsigned();
            $table->foreign('medical_personnel_id')->references('id')->on('medical_personnel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles_medical_personnel', function ($table) {
            $table->dropForeign(['emergency_vehicle_id']);
            $table->dropForeign(['medical_personnel_id']);
        });
        Schema::dropIfExists('vehicles_medical_personnel');
    }
}
