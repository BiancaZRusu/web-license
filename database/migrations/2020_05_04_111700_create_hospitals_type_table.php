<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('type_of_victim_id')->unsigned();
            $table->foreign('type_of_victim_id')->references('id')->on('type_of_victims');
            $table->timestamps();
        });
        Schema::table('hospitals', function ($table) {
            $table->foreign('type_id')->references('id')->on('hospitals_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hospitals_type', function ($table) {
            $table->dropForeign(['type_of_victim_id']);
        });
        Schema::table('hospitals', function ($table) {
            $table->dropForeign(['type_id']);
        });
        Schema::dropIfExists('hospitals_type');
    }
}
