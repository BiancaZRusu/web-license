<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmergencyVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('emergency_vehicles')) {
            Schema::create('emergency_vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('number_of_passengers');
                $table->text('description')->nullable();
                $table->string('name');
                $table->string('medical_attending');
                $table->integer('vehicle_type_id')->unsigned();
                $table->integer('total_number_of_km');
                $table->date('fabrication_date')->nullable();
                $table->timestamps();
            });
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emergency_vehicles');
    }
}
