<?php

use Illuminate\Database\Seeder;

class EmergencyVehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta consult domiciliu'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 2,
            'description'           => 'Ambulanta consult domiciliu pentru medicii de garda in vederea efectuarii consultatiilor la domiciliu.',
            'name'                  => 'Ambulanta consult domiciliu',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip A1'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 1,
            'description'           => 'Ambulanta destinata transportului sanitar neasistat al unui singur pacient, fiind dotata cu echipamentele si materialele minime necesare acordarii primului ajutor in caz de nevoie.',
            'name'                  => 'Ambulanta tip A1',
            'medical_attending'     => 'No',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip A2'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 2,
            'description'           => 'Ambulanta destinata transportului sanitar neasistat al unuia sau al mai multor pacienti pe targa si/sau scaune, fiind dotata cu echipamentele si materialele minime necesare acordarii primului ajutor in caz de nevoie.',
            'name'                  => 'Ambulanta tip A2',
            'medical_attending'     => 'No',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip B'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 2,
            'description'           => 'Ambulanta destinata interventiei de urgenta si transportului medical asistat al pacientilor. Ea poate fi, dupa caz, utilizata in acordarea primului ajutor calificat sau in acordarea asistentei medicale de urgenta. Dotarea ambulantei tip B este formata din echipamente si materiale sanitare care includ, dupa caz, un defibrilator semiautomat sau un defibrilator manual si medicamentele necesare resuscitarii si acordarii asistentei medicale de urgenta.',
            'name'                  => 'Ambulanta tip B',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip C'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 1,
            'description'           => 'Ambulanta destinata interventiei medicale de urgenta la cel mai inalt nivel si transportului medical asistat al pacientului critic, fiind dotata cu echipamente, materiale si medicamente de terapie intensiva. Echipajul ambulantei tip C este condus obligatoriu de un medic special pregatit, iar vehiculul este astfel construit incat sa permita acordarea asistentei medicale de urgenta in mod corespunzator. Ambulantele de transport al nou-nascutilor aflati in stare critica fac parte din categoria ambulantelor tip C.',
            'name'                  => 'Ambulanta tip C',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 3,
            'description'           => 'Mijlocul utilizat: ambulanţe tip B2 sau nave. Conducător al mijlocului de intervenţie cu instruire paramedicală',
            'name'                  => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 3,
            'description'           => 'Mijlocul utilizat: ambulanţe tip B2 sau nave. Conducător al mijlocului de intervenţie cu instruire paramedicală',
            'name'                  => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipaj de terapie intensiva mobila (TIM) . Unitatea mobila de reanimare si terapie intensiva.'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 2,
            'description'           => 'Mijlocul utilizat: ambulanţe tip C1. ',
            'name'                  => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipaj de terapie intensiva mobila nou-nascuti (TIM-NN) mijlocul utilizat'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 3,
            'description'           => 'Ambulanţe tip C1 cu dotare specifică transportului neonatal',
            'name'                  => 'Echipaj de terapie intensiva mobila nou-nascuti (TIM-NN) mijlocul utilizat',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Unitati de interventii speciale si transport medicalizat'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 2,
            'description'           => '',
            'name'                  => 'Unitati de interventii speciale si transport medicalizat',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
    }
}
