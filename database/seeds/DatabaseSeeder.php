<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // medical_personnel table
        DB::table('medical_personnel')->updateOrInsert([
            'name' => 'Medic de urgenta sau medic de anestezie terapie intensivă'
        ],[
            'medical_training'  => 'Specialist sau rezident cel puţin în semestrul II al anului III de rezidenţiat, cu acordul directorului programului de rezidenţiat ori al îndrumătorului, în cazul rezidenţilor în anestezie terapie intensivă, cu experienţă spitalicească continuă, instruit în utilizarea medicaţiei anestezice',
            'name'              => 'Medic de urgenta sau medic de anestezie terapie intensivă',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('medical_personnel')->updateOrInsert([
            'name' => 'Asistent medical de urgenta'
        ],[
            'medical_training'  => 'Pregătire în acordarea asistenţei medicale de urgenţă cu experienţă spitalicească în manevrele de urgenţă şi terapie intensivă şi utilizarea medicaţiei anestezice',
            'name'              => 'Asistent medical de urgenta',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('medical_personnel')->updateOrInsert([
            'name' => 'Persoană cu pregătire paramedicală'
        ],[
            'medical_training'  => 'Care a absolvit cursul de prim ajutor calificat şi de lucru în cadrul unui echipaj medical de urgenţă',
            'name'              => 'Persoană cu pregătire paramedicală',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('medical_personnel')->updateOrInsert([
            'name' => 'Conducător auto pompier'
        ],[
            'medical_training'  => 'Care a absolvit cursul de prim ajutor calificat sau un conducător auto ambulanţier',
            'name'              => 'Conducător auto pompier',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('medical_personnel')->updateOrInsert([
            'name' => 'Medic neonatolog, anestezist reanimator sau de urgenţă'
        ],[
            'medical_training'  => 'Pregătire specifică în acordarea asistenţei medicale de urgenţă şi terapia intensivă a nou-născutului aflat în stare critică, în condiţii de prespital şi transport (specialist sau rezident cel puţin în semestrul II al anului III de rezidenţiat, cu acordul directorului programului de rezidenţiat ori al îndrumătorului, în cazul rezidenţilor în anestezie terapie intensivă sau în neonatologie)',
            'name'              => 'Medic neonatolog, anestezist reanimator sau de urgenţă',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('medical_personnel')->updateOrInsert([
            'name' => 'Asistent medical nou nascuti'
        ],[
            'medical_training'  => 'Pregătire specifică în îngrijirea nou-născutului',
            'name'              => 'Asistent medical nou nascuti',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        // emergency_vehicles table
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta consult domiciliu'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 2,
            'description'           => 'Ambulanta consult domiciliu pentru medicii de garda in vederea efectuarii consultatiilor la domiciliu.',
            'name'                  => 'Ambulanta consult domiciliu',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip A1'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 1,
            'description'           => 'Ambulanta destinata transportului sanitar neasistat al unui singur pacient, fiind dotata cu echipamentele si materialele minime necesare acordarii primului ajutor in caz de nevoie.',
            'name'                  => 'Ambulanta tip A1',
            'medical_attending'     => 'No',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip A2'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 2,
            'description'           => 'Ambulanta destinata transportului sanitar neasistat al unuia sau al mai multor pacienti pe targa si/sau scaune, fiind dotata cu echipamentele si materialele minime necesare acordarii primului ajutor in caz de nevoie.',
            'name'                  => 'Ambulanta tip A2',
            'medical_attending'     => 'No',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip B'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 2,
            'description'           => 'Ambulanta destinata interventiei de urgenta si transportului medical asistat al pacientilor. Ea poate fi, dupa caz, utilizata in acordarea primului ajutor calificat sau in acordarea asistentei medicale de urgenta. Dotarea ambulantei tip B este formata din echipamente si materiale sanitare care includ, dupa caz, un defibrilator semiautomat sau un defibrilator manual si medicamentele necesare resuscitarii si acordarii asistentei medicale de urgenta.',
            'name'                  => 'Ambulanta tip B',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Ambulanta tip C'
        ],[
            'vehicle_type_id'       => 1,
            'number_of_passengers'  => 1,
            'description'           => 'Ambulanta destinata interventiei medicale de urgenta la cel mai inalt nivel si transportului medical asistat al pacientului critic, fiind dotata cu echipamente, materiale si medicamente de terapie intensiva. Echipajul ambulantei tip C este condus obligatoriu de un medic special pregatit, iar vehiculul este astfel construit incat sa permita acordarea asistentei medicale de urgenta in mod corespunzator. Ambulantele de transport al nou-nascutilor aflati in stare critica fac parte din categoria ambulantelor tip C.',
            'name'                  => 'Ambulanta tip C',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 3,
            'description'           => 'Mijlocul utilizat: ambulanţe tip B2 sau nave. Conducător al mijlocului de intervenţie cu instruire paramedicală',
            'name'                  => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 3,
            'description'           => 'Mijlocul utilizat: ambulanţe tip B2 sau nave. Conducător al mijlocului de intervenţie cu instruire paramedicală',
            'name'                  => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipaj de terapie intensiva mobila (TIM) . Unitatea mobila de reanimare si terapie intensiva.'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 2,
            'description'           => 'Mijlocul utilizat: ambulanţe tip C1. ',
            'name'                  => 'Echipajul de prim-ajutor cu capacitate de evacuare a victimei (PA-T)',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Echipaj de terapie intensiva mobila nou-nascuti (TIM-NN) mijlocul utilizat'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 3,
            'description'           => 'Ambulanţe tip C1 cu dotare specifică transportului neonatal',
            'name'                  => 'Echipaj de terapie intensiva mobila nou-nascuti (TIM-NN) mijlocul utilizat',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('emergency_vehicles')->updateOrInsert([
            'name' => 'Unitati de interventii speciale si transport medicalizat'
        ],[
            'vehicle_type_id'       => 2,
            'number_of_passengers'  => 2,
            'description'           => '',
            'name'                  => 'Unitati de interventii speciale si transport medicalizat',
            'medical_attending'     => 'Yes',
            'total_number_of_km'    => 0,
            'fabrication_date'      => null,
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        // automobile table
        DB::table('automobile')->updateOrInsert([
            'name' => 'Transport personal si multiple victime'
        ],[
            'description'       => '',
            'emergency_vehicle_id'  => 9,
            'name'              => 'Transport personal si multiple victime',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile')->updateOrInsert([
            'name' => 'Masina de comanda'
        ],[
            'description'       => 'Centrul de comanda SMURD este o masina care poate fi folosita in situatii extreme de genul catastrofelor naturale, accidentelor aviatice, feroviare etc',
            'emergency_vehicle_id'  => 9,
            'name'              => 'Masina de comanda',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile')->updateOrInsert([
            'name' => 'Masina de suport logistic'
        ],[
            'description'       => 'Rolul acestei masini este de a sprijini echipajele de prim-ajutor precum si echipajele de pompieri, aflate la locul interventiei. Pe langa transportul materialelor, masina are capacitate de transport pentru 5 persoane, ceea ce face  sa fie utila si in cazul accidentelor colective, ca masini de sprijin logistic la locul interventiei.',
            'emergency_vehicle_id'  => 9,
            'name'              => 'Masina de suport logistic',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile')->updateOrInsert([
            'name' => 'Masina de interventie rapida pentru medic de urgenta'
        ],[
            'description'       => 'Autospeciale utilizate in transportul medicului si al echipajului medical cu echipamentele acestora pina la locul interventiei',
            'emergency_vehicle_id'  => 9,
            'name'              => 'Masina de interventie rapida pentru medic de urgenta',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile')->updateOrInsert([
            'name' => 'Masina de descarcerare'
        ],[
            'description'       => '',
            'emergency_vehicle_id'  => 9,
            'name'              => 'Masina de descarcerare',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile')->updateOrInsert([
            'name' => 'Masina interventie accidente colective si calamitati'
        ],[
            'description'           => '',
            'emergency_vehicle_id'  => 9,
            'name'                  => 'Masina interventie accidente colective si calamitati',
            'created_at'            => now(),
            'updated_at'            => now(),
        ]);
        DB::table('automobile')->updateOrInsert([
            'name' => 'Masina de interventie accidente rutiere'
        ],[
            'description'       => '',
            'emergency_vehicle_id'  => 9,
            'name'              => 'Masina de interventie accidente rutiere',
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        // automobile_features table
        DB::table('automobile_features')->updateOrInsert([
            'description' => 'Masina de suport logistic este dotata cu butelii de oxigen portabile, butelii de oxigen de capacitate mare pentru uzul interior al ambulantelor si containere pentru materialele consumabile'
        ],[
            'description'       => 'Masina de suport logistic este dotata cu butelii de oxigen portabile, butelii de oxigen de capacitate mare pentru uzul interior al ambulantelor si containere pentru materialele consumabile',
            'automobile_id'     => 3,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile_features')->updateOrInsert([
            'description' => 'Fiecare dintre masinile de suport logistic are in dotare butelii de aer comprimat si un compresor de aer, necesare pompierilor in timpul interventiilor la incendii (astfel se asigura schimbul buteliilor utilizate de pompieri si reumplerea lor la locul interventiei)'
        ],[
            'description'       => 'Fiecare dintre masinile de suport logistic are in dotare butelii de aer comprimat si un compresor de aer, necesare pompierilor in timpul interventiilor la incendii (astfel se asigura schimbul buteliilor utilizate de pompieri si reumplerea lor la locul interventiei)',
            'automobile_id'     => 3,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile_features')->updateOrInsert([
            'description' => 'Este o autospeciala dotata cu echipamente destinate descarcerarii si interventiilor speciale de salvare'
        ],[
            'description'       => 'Este o autospeciala dotata cu echipamente destinate descarcerarii si interventiilor speciale de salvare',
            'automobile_id'     => 5,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile_features')->updateOrInsert([
            'description' => 'Echipamentele din dotare includ agregate hidraulice, instrumente speciale de taiat si indepartat, perne speciale de ridicat greutati de pina la 30 de tone utilizind aerul comprimat, generatoare electrice si reflectoare ce permit iluminarea zonei de interventie'
        ],[
            'description'       => 'Echipamentele din dotare includ agregate hidraulice, instrumente speciale de taiat si indepartat, perne speciale de ridicat greutati de pina la 30 de tone utilizind aerul comprimat, generatoare electrice si reflectoare ce permit iluminarea zonei de interventie',
            'automobile_id'     => 5,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile_features')->updateOrInsert([
            'description' => 'Echipamentele din dotare includ agregate hidraulice, instrumente speciale de taiat si indepartat, perne speciale de ridicat greutati de pina la 30 de tone utilizind aerul comprimat, generatoare electrice si reflectoare ce permit iluminarea zonei de interventie'
        ],[
            'description'       => 'Echipamentele din dotare includ agregate hidraulice, instrumente speciale de taiat si indepartat, perne speciale de ridicat greutati de pina la 30 de tone utilizind aerul comprimat, generatoare electrice si reflectoare ce permit iluminarea zonei de interventie',
            'automobile_id'     => 3,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('automobile_features')->updateOrInsert([
            'description' => 'Masina este prevazuta cu echipamente pentru acordarea sprijinul logistic si transportului de materiale si oxigen pentru echipajele de prim-ajutor astfel incat sa nu fie necesara deplasarea echipajelor si intreruperea activitatii in vederea aprovizionarii'
        ],[
            'description'       => 'Masina este prevazuta cu echipamente pentru acordarea sprijinul logistic si transportului de materiale si oxigen pentru echipajele de prim-ajutor astfel incat sa nu fie necesara deplasarea echipajelor si intreruperea activitatii in vederea aprovizionarii',
            'automobile_id'     => 3,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        // automobile_features table
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 6,
            'medical_personnel_id'     => 4,
        ],[
            'emergency_vehicle_id'     => 6,
            'medical_personnel_id'     => 4,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 1,
        ],[
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 1,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 2,
        ],[
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 2,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 3,
        ],[
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 3,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 4,
        ],[
            'emergency_vehicle_id'     => 7,
            'medical_personnel_id'     => 4,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 8,
            'medical_personnel_id'     => 5,
        ],[
            'emergency_vehicle_id'     => 8,
            'medical_personnel_id'     => 5,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 8,
            'medical_personnel_id'     => 4,
        ],[
            'emergency_vehicle_id'     => 8,
            'medical_personnel_id'     => 4,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        DB::table('vehicles_medical_personnel')->updateOrInsert([
            'emergency_vehicle_id'     => 8,
            'medical_personnel_id'     => 6,
        ],[
            'emergency_vehicle_id'     => 8,
            'medical_personnel_id'     => 6,
            'created_at'        => now(),
            'updated_at'        => now(),
        ]);
        // vehicle_type
        DB::table('vehicle_type')->updateOrInsert([
            'name' => 'smurd'
        ],[
            'name' => 'smurd',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('vehicle_type')->updateOrInsert([
            'name' => 'ambulance'
        ],[
            'name' => 'ambulance',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Serviciul Județean de Ambulanță Iași'
        ],[
            'name' => 'Serviciul Județean de Ambulanță Iași',
            'city' => 'Iași',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.6000',
            'latitude' => '47.1530',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Târgu Frumos'
        ],[
            'name' => 'Ambulanţa Târgu Frumos',
            'city' => 'Târgu Frumos',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.0126',
            'latitude' => '47.2110',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Inspectoratul pentu Situații de Urgență Mihail Grigore Sturza al județului Iași'
        ],[
            'name' => 'Inspectoratul pentu Situații de Urgență Mihail Grigore Sturza al județului Iași',
            'city' => 'Iasi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.5725',
            'latitude' => '47.1777',
            'vehicle_type_id' => 2
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Inspectoratul pentu Situații de Urgență Botosani'
        ],[
            'name' => 'Inspectoratul pentu Situații de Urgență Botosani',
            'city' => 'Botosani',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.6478',
            'latitude' => '47.7519',
            'vehicle_type_id' => 2
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Inspectoratul pentu Situații de Urgență Suceava'
        ],[
            'name' => 'Inspectoratul pentu Situații de Urgență Suceava',
            'city' => 'Suceava',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.2480',
            'latitude' => '47.6381',
            'vehicle_type_id' => 2
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Inspectoratul pentu Situații de Urgență Neamt'
        ],[
            'name' => 'Inspectoratul pentu Situații de Urgență Neamt',
            'city' => 'Neamt',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.3691',
            'latitude' => '46.9252',
            'vehicle_type_id' => 2
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Inspectoratul pentu Situații de Urgență Vaslui'
        ],[
            'name' => 'Inspectoratul pentu Situații de Urgență Vaslui',
            'city' => 'Vaslui',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.7200',
            'latitude' => '46.6448',
            'vehicle_type_id' => 2
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Inspectoratul pentu Situații de Urgență Bacau'
        ],[
            'name' => 'Inspectoratul pentu Situații de Urgență Bacau',
            'city' => 'Bacau',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.9225',
            'latitude' => '46.5514',
            'vehicle_type_id' => 2
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Botosani'
        ],[
            'name' => 'Ambulanţa Botosani',
            'city' => 'Botosani',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.6613',
            'latitude' => '47.7415',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Dorohoi'
        ],[
            'name' => 'Ambulanţa Dorohoi',
            'city' => 'Dorohoi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.4035',
            'latitude' => '47.9457',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Radauti'
        ],[
            'name' => 'Ambulanţa Radauti',
            'city' => 'Radauti',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '25.9117',
            'latitude' => '47.8413',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Vatra Dornei'
        ],[
            'name' => 'Ambulanţa Vatra Dornei',
            'city' => 'Vatra Dornei',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '25.3588',
            'latitude' => '47.3478',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Campulung'
        ],[
            'name' => 'Ambulanţa Campulung',
            'city' => 'Campulung',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '25.5441',
            'latitude' => '47.5326',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Campulung'
        ],[
            'name' => 'Ambulanţa Campulung',
            'city' => 'Campulung',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '25.5441',
            'latitude' => '47.5326',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Gura Humorului'
        ],[
            'name' => 'Ambulanţa Gura Humorului',
            'city' => 'Gura Humorului',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '25.8926',
            'latitude' => '47.5543',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Suceava'
        ],[
            'name' => 'Ambulanţa Suceava',
            'city' => 'Suceava',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.2406',
            'latitude' => '47.6403',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Falticeni'
        ],[
            'name' => 'Ambulanţa Falticeni',
            'city' => 'Falticeni',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.3058',
            'latitude' => '47.4612',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Bicaz'
        ],[
            'name' => 'Ambulanţa Bicaz',
            'city' => 'Bicaz',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.0849',
            'latitude' => '46.9200',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Neamt'
        ],[
            'name' => 'Ambulanţa Neamt',
            'city' => 'Neamt',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.3875',
            'latitude' => '46.9161',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Mircesti'
        ],[
            'name' => 'Ambulanţa Mircesti',
            'city' => 'Mircesti',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.8432',
            'latitude' => '47.0619',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Pascani'
        ],[
            'name' => 'Ambulanţa Pascani',
            'city' => 'Pascani',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.7265',
            'latitude' => '47.2453',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Targu Frumos'
        ],[
            'name' => 'Ambulanţa Targu Frumos',
            'city' => 'Targu Frumos',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.7265',
            'latitude' => '47.2453',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Targu Frumos'
        ],[
            'name' => 'Ambulanţa Targu Frumos',
            'city' => 'Targu Frumos',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.0126',
            'latitude' => '47.2110',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Raducaneni'
        ],[
            'name' => 'Ambulanţa Raducaneni',
            'city' => 'Raducaneni',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.9370',
            'latitude' => '46.9608',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Husi'
        ],[
            'name' => 'Ambulanţa Husi',
            'city' => 'Husi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '28.0617',
            'latitude' => '46.6723',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Vaslui'
        ],[
            'name' => 'Ambulanţa Vaslui',
            'city' => 'Vaslui',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.7270',
            'latitude' => '46.6443',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Barlad'
        ],[
            'name' => 'Ambulanţa Barlad',
            'city' => 'Barlad',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.6786',
            'latitude' => '46.2451',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Bacau'
        ],[
            'name' => 'Ambulanţa Bacau',
            'city' => 'Bacau',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.9162',
            'latitude' => '46.5687',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Comanesti'
        ],[
            'name' => 'Ambulanţa Comanesti',
            'city' => 'Comanesti',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.4466',
            'latitude' => '46.4183',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Moinesti'
        ],[
            'name' => 'Ambulanţa Moinesti',
            'city' => 'Moinesti',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.4830',
            'latitude' => '46.4677',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Buhusi'
        ],[
            'name' => 'Ambulanţa Buhusi',
            'city' => 'Buhusi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.7089',
            'latitude' => '46.7200',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Onesti'
        ],[
            'name' => 'Ambulanţa Onesti',
            'city' => 'Onesti',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.7650',
            'latitude' => '46.2535',
            'vehicle_type_id' => 1
        ]);
        DB::table('emergency_locations')->updateOrInsert([
            'name' => 'Ambulanţa Targu Ocna'
        ],[
            'name' => 'Ambulanţa Targu Ocna',
            'city' => 'Targu Ocna',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '26.6117',
            'latitude' => '46.2776',
            'vehicle_type_id' => 1
        ]);
        DB::table('type_of_victims')->updateOrInsert([
            'type' => 'Insarcinata'
        ],[
            'type' => 'Insarcinata',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('type_of_victims')->updateOrInsert([
            'type' => 'Nu se specifica'
        ],[
            'type' => 'Nu se specifica',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('type_of_victims')->updateOrInsert([
            'type' => 'Copil'
        ],[
            'type' => 'Copil',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('hospitals_type')->updateOrInsert([
            'name' => 'emergency_hospital'
        ],[
            'name' => 'emergency_hospital',
            'type_of_victim_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('hospitals_type')->updateOrInsert([
            'name' => 'children_hospital'
        ],[
            'name' => 'children_hospital',
            'type_of_victim_id' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('hospitals_type')->updateOrInsert([
            'name' => 'maternity_hospital'
        ],[
            'name' => 'maternity_hospital',
            'type_of_victim_id' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('hospitals_type')->updateOrInsert([
            'name' => 'infectious_diseases_hospital'
        ],[
            'name' => 'infectious_diseases_hospital',
            'type_of_victim_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('hospitals')->updateOrInsert([
            'name' => 'Spitalul Clinic de Urgență pentru Copii "Sfânta Maria"'
        ],[
            'name' => 'Spitalul Clinic de Urgență pentru Copii "Sfânta Maria"',
            'city' => 'Iasi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.6079137',
            'latitude' => '47.1596966',
            'type_id' => 2
        ]);
        DB::table('hospitals')->updateOrInsert([
            'name' => 'Spitalul de Obstetrica - Ginecologie "Elena Doamna"'
        ],[
            'name' => 'Spitalul de Obstetrica - Ginecologie "Elena Doamna"',
            'city' => 'Iasi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.5979934',
            'latitude' => '47.1606314',
            'type_id' => 3
        ]);
        DB::table('hospitals')->updateOrInsert([
            'name' => 'Spitalul de Boli Infecțioase Sfinta Cuvioasa Paraschiva'
        ],[
            'name' => 'Spitalul de Boli Infecțioase Sfinta Cuvioasa Paraschiva',
            'city' => 'Iasi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.5845403',
            'latitude' => '47.1705879',
            'type_id' => 4
        ]);
        DB::table('hospitals')->updateOrInsert([
            'name' => 'Spitalul Sfântul Spiridon'
        ],[
            'name' => 'Spitalul Sfântul Spiridon',
            'city' => 'Iasi',
            'created_at' => now(),
            'updated_at' => now(),
            'longitude' => '27.5827081',
            'latitude' => '47.1685977',
            'type_id' => 1
        ]);
        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Accident de circulatie soldat cu victime sau cu persoane blocate in autovehicule'
        ],[
            'name' => 'Accident de circulatie soldat cu victime sau cu persoane blocate in autovehicule',
            'type_of_emergency_vehicle' => '2,3,4,7,6,5',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Incendiu'
        ],[
            'name' => 'Incendiu',
            'type_of_emergency_vehicle' => '2,3,4',
            'type_of_hospitals' => '1',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Explozii'
        ],[
            'name' => 'Explozii',
            'type_of_emergency_vehicle' => '2,3,4,6',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Electrocutari'
        ],[
            'name' => 'Electrocutari',
            'type_of_emergency_vehicle' => '2, 3, 4',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Caderi de la inaltime'
        ],[
            'name' => 'Caderi de la inaltime',
            'type_of_emergency_vehicle' => '2,3,4,6',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Accident grav la metrou'
        ],[
            'name' => 'Accident grav la metrou',
            'type_of_emergency_vehicle' => '2,3,4',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Accident de aviatie'
        ],[
            'name' => 'Accident de aviatie',
            'type_of_emergency_vehicle' => '4,5',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Accident feroviar'
        ],[
            'name' => 'Accident feroviar',
            'type_of_emergency_vehicle' => '4,5',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Probleme medicale grave'
        ],[
            'name' => 'Probleme medicale grave',
            'type_of_emergency_vehicle' => '1,4,5',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Travaliu'
        ],[
            'name' => 'Travaliu',
            'type_of_emergency_vehicle' => '8',
            'type_of_hospitals' => '3',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Accident nautic'
        ],[
            'name' => 'Accident nautic',
            'type_of_emergency_vehicle' => '1,2,3,6',
            'type_of_hospitals' => '1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('emergency_type')->updateOrInsert([
            'name' => 'Boli infecțioase grave'
        ],[
            'name' => 'Boli infecțioase grave',
            'type_of_emergency_vehicle' => '3,9',
            'type_of_hospitals' => '4',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
