<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// home
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//emergency locations
Route::get('/map', 'MapController@index')->name('map');

//reports
Route::get('/reports', 'ReportsController@index')->name('reports');

//accidents
Route::get('/accident/new', 'AccidentController@index')->name('newAccident');
Route::post('/accident/create', 'AccidentController@create')->name('createAccident');
Route::post('/accident/update/{id}', 'AccidentController@update')->name('updateAccident');
Route::get('/accident/road/{id}', 'AccidentController@nearestAmbulance')->name('road');
Route::get('/accident/{id}', 'AccidentController@accident')->name('accident');
