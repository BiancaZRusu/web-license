var map;

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.1562327, lng: 27.5169309},
        zoom: 8,
        mapTypeId: 'roadmap'
    });
}