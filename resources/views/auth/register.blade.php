@extends('layouts.app')
<link rel="stylesheet" type="text/css" href="{{ asset('css/auth.css') }}">
@section('content')
    <div class="container register d-flex justify-content-center h-100">
        <div class="row register-form">
            <div class="col-md-3 register-left">
                <div class="ambulance-animation"><i class="fas fa-ambulance fa-2x"></i></div>
                <h3>Welcome</h3>
                <input type="submit" name="" value="Login"  class="btn login_btn" onclick="location.href='{{ route('login') }}'"/><br/>
            </div>
            <div class="col-md-9 register-right">
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                <div class="tab-content" id="myTabContent">
                    <div class="tab-paneshow active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row register-form">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" name="name" placeholder="Name *" value="{{ old('name') }}" required autofocus/>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input type="text" class="form-control" name="email" placeholder="Email *" value="{{ old('email') }}" />
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control" placeholder="Password *" value="" name="password" required />
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" required class="form-control"  placeholder="Confirm Password *" value="" />
                                </div>
                                <input type="submit" class="btn btnRegister"  value="Register"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
