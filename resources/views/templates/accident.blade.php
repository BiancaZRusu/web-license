@extends('layouts.app')
@push('css-plugins')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/accident.css') }}">
@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div itemscope>
                    <div class="jumbotron p-3 p-md-5">
                        <div class="col-md-12 px-0">
                            <h3 class="display-4 font-italic">Accident (data <span itemprop="date">{{$accident->created_at}}</span>)</h3>
                            <div itemprop="accident" itemscope class="lead my-3">
                                <div>
                                    <strong>Nume:</strong> <span itemprop="name">{{$accident->name}}</span>
                                </div>
                                <div>
                                    <strong>Urgenta:</strong> <span itemprop="name">{{$accident->emergency_type}}</span>
                                </div>
                                <div>
                                    <strong>Descriere:</strong> <span itemprop="name">{{$accident->description}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="card flex-md-row mb-4 box-shadow h-md-250">
                                <div class="card-body d-flex flex-column align-items-start" itemprop="ambulance" itemscope>
                                    <strong class="d-inline-block mb-2 text-primary">Baza de ambulanta</strong>
                                    <h3 class="mb-0">
                                        <a class="text-dark" href="#" itemprop="name">{{$accident->ambulance_name}}</a>
                                    </h3>
                                    <div class="mb-1 text-muted" itemprop="city">{{$accident->ambulance_city}}</div>
                                    <p class="card-text mb-auto"><strong>Distanta parcursa (Ambulanta - Urgenta):</strong> {{$accident->ambulance_distance_km}}</p>
                                    <p class="card-text mb-auto"><strong>Timpul estimat pana la destinatie:</strong> {{$accident->ambulance_distance_traffic}}</p>
                                </div>
                                <div style="width: 200px; height: 250px;" id="ambulance-map"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card flex-md-row mb-4 box-shadow h-md-250">
                                <div class="card-body d-flex flex-column align-items-start">
                                    <strong class="d-inline-block mb-2 text-success" itemprop="hospital" itemscope>Spital</strong>
                                    <h3 class="mb-0">
                                        <a class="text-dark" href="#" itemprop="name">{{$accident->hospital_name}}</a>
                                    </h3>
                                    <div class="mb-1 text-muted" itemprop="city">{{$accident->hospital_city}}</div>
                                    <p class="card-text mb-auto"><strong>Distanta parcursa (Urgenta - Spital):</strong> {{$accident->hospital_distance_km}}</p>
                                    <p class="card-text mb-auto"><strong>Timpul estimat pana la destinatie:</strong> {{$accident->hospital_distance_traffic}}</p>
                                </div>
                                <div style="width: 200px; height: 250px;" id="hospital-map"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&libraries=places&sensor=false"></script>
    <script>
            var accident = {!! json_encode($accident, JSON_HEX_TAG) !!};
            if (accident.ambulance_type == 1) {
                var icon = "../images/ambulance_marker.svg";
                var title = 'Ambulanta';
            } else {
                var icon = "../images/isu_marker.svg";
                var title = 'SMURD';
            }
            ambulanceLocation = {lat: parseFloat(accident.ambulance_latitude), lng:  parseFloat(accident.ambulance_longitude)};
            initMap(ambulanceLocation, 'ambulance-map', icon, title, accident.ambulance_name);
            hospitalLocation = {lat: parseFloat(accident.hospital_latitude), lng:  parseFloat(accident.hospital_longitude)};
            initMap(ambulanceLocation, 'hospital-map', "../images/hospital_marker.png", "Spital", accident.hospital_name);

        function initMap(location, mapId, icon, title, name) {
            let map = new google.maps.Map(document.getElementById(mapId), {
                center: location,
                zoom: 8
            });
            var marker = new google.maps.Marker({
                position: location,
                icon: icon,
                map: map,
                title: title
            });
            var infowindow = new google.maps.InfoWindow({
                content: name
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        }
    </script>
@endsection