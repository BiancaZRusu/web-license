@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <form method="POST" action="{{ route('createAccident') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12">
                            <h1>Accident</h1>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-name">Nume</label>
                        <input name="name" type="text" class="form-control" id="input-name" placeholder="Nume" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="input-phone">Telefon</label>
                        <input name="phone" type="text" class="form-control" id="input-phone" placeholder="Telefon" required>
                    </div>
                    <div class="form-group">
                        <label for="input-emergency">Urgenta</label>
                        <select id="emergency" class="form-control" name="emergency" >
                            @foreach($emergencyType as $emergency)
                                <option value="{{ $emergency->id }}">{{ $emergency->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="input-description">Descriere</label>
                        <input name="description" type="text" class="form-control" id="input-description" placeholder="Descriere" required>
                    </div>
                    <div class="form-group">
                        <label for="input-victim">Persoana implicata</label>
                        <select id="cars" class="form-control" name="type_of_victim" >
                            @foreach($victimsType as $type)
                                <option value="{{ $type->id }}">{{ $type->type }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="input-nr-victim">Numarul de victime</label>
                        <input name="victims_number" type="text" class="form-control" id="input-nr-victim" placeholder="Numarul de victime" required>
                    </div>
                    <div class="form-group">
                        <label for="input-location">Locatie</label>
                        <input name="latitude" type="hidden" name="address_latitude" id="input-latitude" value="0" />
                        <input name="longitude" type="hidden" name="address_longitude" id="input-longitude" value="0" />
                        <input name="location_details" type="hidden" name="address_detail" id="input-address-detail" value="0" />
                        <input name="city" type="hidden" name="address_city" id="input-address-city" value="0" />
                        <div id="address-map-container" style="width:100%;height:300px; ">
                            <input style="width: 300px; height: 30px;" id="pac-input" class="controls" type="text" placeholder="Search Box">
                            <div style="width: 100%; height: 100%" id="map"></div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Adauga</button>
                </form>
            </main>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&libraries=places&sensor=false"></script>
    <script src="{{ asset('js/mapInput.js') }}"></script>
@endsection