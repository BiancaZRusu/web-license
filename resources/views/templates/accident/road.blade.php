@extends('layouts.app')
@push('css-plugins')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/accident.css') }}">
@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div class="loader" style="display: block"></div>
                <div class="map-container" style="display: none">
                    <div id="right-panel"></div>
                    <div style="width: auto; height: 800px;" id="map">
                    </div>
                    <div class="road-marker">
                        <img jstcache="35" src="../../images/marker_a.png">
                        <p id="road-marker-a" style="display: inline-block;">- Ambulanta</p>
                    </div>
                    <div class="road-marker">
                        <img jstcache="35" src="../../images/marker_b.png">
                        <p id="road-marker-b" style="display: inline-block;">- Urgenta</p>
                    </div>
                    <div class="road-marker">
                        <img jstcache="35" src="../../images/marker_c.png">
                        <p id="road-marker-c" style="display: inline-block;">- Spital</p>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&libraries=places&sensor=false"></script>
    <script>
        let accident = {!! json_encode($accident, JSON_HEX_TAG) !!};
        let emergencyLocations = {!! json_encode($emergencyLocations, JSON_HEX_TAG) !!};
        let hospitalsLocations = {!! json_encode($hospitals, JSON_HEX_TAG) !!};
        let locationGpsAccident = {lat: parseFloat(accident.latitude), lng: parseFloat(accident.longitude)};
        let minDistanceLocation = locationGpsAccident;
        let accidentLocationName = "";
        let result;
        let ambulance;
        let hospital;
        let service = new google.maps.DistanceMatrixService();
        let TransitOptions = {
            routingPreference: 'LESS_WALKING'
        };
        if (accident.hospital_id && accident.ambulance_id) {
            ambulance = emergencyLocations[0];
            hospital = hospitalsLocations[0];
            ambulance.location = {
                lat: parseFloat(ambulance.latitude),
                lng: parseFloat(ambulance.longitude)
            };
            hospital.location = {
                lat: parseFloat(hospital.latitude),
                lng: parseFloat(hospital.longitude)
            };
            document.getElementById('road-marker-a').innerText = " - Ambulanta (" + ambulance.name + ")";
            document.getElementById('road-marker-b').innerText = " - Urgenta (" + accident.description + ")";
            document.getElementById('road-marker-c').innerText = " - Spital (" + hospital.name + ")";
            initMap();
            document.getElementsByClassName('loader')[0].style.display = "none";
            document.getElementsByClassName('map-container')[0].style.display = "block";
        } else {
            getDistanceTime(emergencyLocations, locationGpsAccident);
            setTimeout(() => {
                ambulance = result;
                getDistanceTime(hospitalsLocations, locationGpsAccident);
                setTimeout(() => {
                    hospital = result;
                    document.getElementById('road-marker-a').innerText = " - Ambulanta (" + ambulance.name + ")";
                    document.getElementById('road-marker-b').innerText = " - Urgenta (" + accident.description + ")";
                    document.getElementById('road-marker-c').innerText = " - Spital (" + hospital.name + ")";
                    initMap();
                    document.getElementsByClassName('loader')[0].style.display = "none";
                    document.getElementsByClassName('map-container')[0].style.display = "block";
                    saveNearestAmbulanceAndHospital(ambulance, hospital);
                }, 500);
            }, 500);
        }

        function initMap() {
            let directionsRenderer = new google.maps.DirectionsRenderer;
            let directionsService = new google.maps.DirectionsService;
            let map = new google.maps.Map(document.getElementById('map'), {
                center: locationGpsAccident,
                zoom: 8
            });
            directionsRenderer.setMap(map);
            calculateAndDisplayRoute(map, directionsService, directionsRenderer, ambulance.location, hospital.location, locationGpsAccident);
            directionsRenderer.setPanel(document.getElementById('right-panel'));
        }

        function calculateAndDisplayRoute(map, directionsService, directionsRenderer, origin, destination, waypts) {
            let wayP = [];
            wayP.push({
                'location': waypts,
                stopover: true
            });
            let selectedMode = "DRIVING";
            directionsService.route({
                origin: {lat: origin.lat, lng: origin.lng},
                destination: {lat: destination.lat, lng: destination.lng},
                travelMode: google.maps.TravelMode[selectedMode],
                waypoints: wayP,
                drivingOptions: {
                    departureTime: new Date(Date.now()),
                    trafficModel: 'bestguess'
                }
            }, function (response, status) {
                if (status == 'OK') {
                    directionsRenderer.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });
        }

        function nearestHospitalOrAmbulance(locations, locationGpsAccident) {
            let minDistance = 10000;
            let locationName = "";
            jQuery.each(locations, function allEm(index, item) {
                let location = {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)};
                let distance = calcCrow(locationGpsAccident.lat, locationGpsAccident.lng, location.lat, location.lng)
                if (minDistance > distance) {
                    minDistance = distance;
                    minDistanceLocation = location;
                    locationName = item.name + ", " + item.city;
                }
            });
            let result = {
                'location': minDistanceLocation,
                'name': locationName,
            }
            return result;
        }

        async function getDistanceTime(locations, locationGpsAccident) {
            let minDistance = 10000;
            let locationName = "";
            jQuery.each(locations, function allEm(index, item) {
                service.getDistanceMatrix(
                    {
                        origins: [{lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)}],
                        destinations: [locationGpsAccident],
                        travelMode: 'DRIVING',
                        drivingOptions: {
                            departureTime: new Date(Date.now()),
                            trafficModel: 'bestguess'
                        }
                    }, function (response, status) {
                        if (status == 'OK') {
                            duration = response.rows[0].elements[0].duration_in_traffic.value;
                            if (minDistance > duration) {
                                minDistance = duration;
                                minDistanceLocation = {
                                    lat: parseFloat(item.latitude),
                                    lng: parseFloat(item.longitude)
                                };
                                locationName = item.name + ", " + item.city;
                                result = {
                                    'location': minDistanceLocation,
                                    'name': locationName,
                                    'location_id': item.id,
                                    'duration_in_traffic': response.rows[0].elements[0].duration_in_traffic.text,
                                    'distance': response.rows[0].elements[0].distance.text,
                                };
                            }
                        } else {
                            window.alert('Directions request failed due to ' + status);
                        }
                    });
            });
        }

        function saveNearestAmbulanceAndHospital(ambulance, hospital) {
            $.ajax({
                type: "POST",
                url: '../update/' + accident.id,
                data: {
                    hospital_id: hospital.location_id,
                    hospital_distance_km: hospital.distance,
                    hospital_distance_traffic: hospital.duration_in_traffic,
                    ambulance_id: ambulance.location_id,
                    ambulance_distance_km: ambulance.distance,
                    ambulance_distance_traffic: ambulance.duration_in_traffic,
                    _token: '{{csrf_token()}}'
                },
                success: function (data) {
                    // console.log(data);
                },
                error: function (data, textStatus, errorThrown) {
                    console.log(data);
                },
            });
        }

        function haversine_distance(mk1, mk2) {
            let R = 3958.8; // Radius of the Earth in miles
            let rlat1 = mk1.lat * (Math.PI / 180); // Convert degrees to radians
            let rlat2 = mk2.lat * (Math.PI / 180); // Convert degrees to radians
            let difflat = rlat2 - rlat1; // Radian difference (latitudes)
            let difflon = (mk2.lng - mk1.lng) * (Math.PI / 180); // Radian difference (longitudes)

            let d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat / 2) * Math.sin(difflat / 2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.sin(difflon / 2) * Math.sin(difflon / 2)));
            return d;
        }

        //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
        function calcCrow(lat1, lon1, lat2, lon2) {
            let R = 6371; // km
            let dLat = toRad(lat2 - lat1);
            let dLon = toRad(lon2 - lon1);
            lat1 = toRad(lat1);
            lat2 = toRad(lat2);

            let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
            let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            let d = R * c;
            return d;
        }

        // Converts numeric degrees to radians
        function toRad(Value) {
            return Value * Math.PI / 180;
        }

    </script>
@endsection