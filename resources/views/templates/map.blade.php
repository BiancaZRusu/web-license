@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
{{--                <div style="width: auto; height: 800px;">--}}
{{--                    {!! Mapper::render() !!}--}}
{{--                </div>--}}
                <div style="width: auto; height: 800px;" id="map"></div>
            </main>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&libraries=places&sensor=false"></script>
    <script>
        var vehicles = {!! json_encode($vehicles, JSON_HEX_TAG) !!};
        var hospitals = {!! json_encode($hospitals, JSON_HEX_TAG) !!};
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 47.1562327, lng:  27.5169309},
            zoom: 8
        });
        jQuery.each(vehicles, function allEm(index, item) {
            var location = {lat: parseFloat(item.latitude), lng:  parseFloat(item.longitude)};
            if (item.vehicle_type_id == 1) {
                var icon = "images/ambulance_marker.svg";
                var title = 'Ambulanta';
            } else {
                var icon = "images/isu_marker.svg";
                var title = 'SMURD';
            }

            var marker = new google.maps.Marker({
                position: location,
                icon: icon,
                map: map,
                title: title
            });
            var infowindow = new google.maps.InfoWindow({
                content: item.name
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        });

        jQuery.each(hospitals, function allEm(index, item) {
            var location = {lat: parseFloat(item.latitude), lng:  parseFloat(item.longitude)};
            var marker = new google.maps.Marker({
                position: location,
                icon: "images/hospital_marker.png",
                map: map,
                title: "Spital"
            });
            var infowindow = new google.maps.InfoWindow({
                content: item.name
            });
            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        })
    </script>
@endsection