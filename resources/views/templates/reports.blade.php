@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <canvas class="my-4 chartjs-render-monitor" id="myChart" width="667" height="281"
                        style="display: block; width: 667px; height: 281px;"></canvas>
            </main>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script>
        var accidents = {!! json_encode($accidents, JSON_HEX_TAG) !!};
        var labelsData = [];
        var totalsData = [];
        var ctx = document.getElementById("myChart");
        jQuery.each(accidents, function allEm(index, item) {
            labelsData.push(item.created_at);
            totalsData.push(item.total_accidents);
        });
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labelsData,
                datasets: [{
                    data: totalsData,
                    lineTension: 0,
                    backgroundColor: 'transparent',
                    borderColor: '#007bff',
                    borderWidth: 4,
                    pointBackgroundColor: '#007bff'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });

    </script>
@endsection
