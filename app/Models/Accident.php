<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Accident extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accidents';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'emergency', 'description','victims_number','type_of_victim','longitude'
        ,'latitude', 'created_at', 'updated_at', 'location_details',
        'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
