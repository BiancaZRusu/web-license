<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mapper;
use DB;

class MapController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $vehicles = DB::table('emergency_locations')->get()->toArray();
        foreach($vehicles as $key => $csm)
        {
            $csmap_data[$key]['flag'] = 1;
        }
        $hospitals = DB::table('hospitals')->get()->toArray();

        return view('templates/map', ["hospitals" => $hospitals, "vehicles" => $vehicles]);
    }
}