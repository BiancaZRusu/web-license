<?php

namespace App\Http\Controllers;

use DB;
use Mapper;
use App\Models\Accident;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class AccidentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $victimsType = DB::table('type_of_victims')
                    ->orderBy('id', 'desc')
                    ->get();
        $emergencyType = DB::table('emergency_type')
            ->get();
        Mapper::map(47.1562327, 27.5169309, ['marker' => false, 'zoom' => 12]);
        return view('templates/accident/form', ["victimsType" => $victimsType, "emergencyType" => $emergencyType]);
    }

    public function create(Request $request)
    {
        $accident = new Accident();
        $data = $request->all();
        $data['created_at'] = now();
        $data['updated_at'] = now();
        $accident->fill($data);
        /** save in db */
        $accident->save();
        $this->sendNotificationToAmbulance($accident);
        return redirect('accident/road/' . $accident->id);
    }

    public function update(Request $request, $id) {
        $data = $request->all();
        $accident = DB::table('accidents')
            ->where('id', $id)
            ->update(
                [
                    'hospital_id' => $data['hospital_id'],
                    'hospital_distance_km' => $data['hospital_distance_km'],
                    'hospital_distance_traffic' => $data['hospital_distance_traffic'],
                    'ambulance_id' => $data['ambulance_id'],
                    'ambulance_distance_km' => $data['ambulance_distance_km'],
                    'ambulance_distance_traffic' => $data['ambulance_distance_traffic'],
                ]
            );
        return $accident;
    }

    public function accident(Request $request, $id) {
        $accident = DB::table('accidents')
            ->join('type_of_victims', 'type_of_victims.id', '=', 'accidents.type_of_victim')
            ->join('emergency_type', 'emergency_type.id', '=', 'accidents.emergency')
            ->join('hospitals', 'hospitals.id', '=', 'accidents.hospital_id')
            ->join('emergency_locations', 'emergency_locations.id', '=', 'accidents.ambulance_id')
            ->select('accidents.*', 'type_of_victims.type as victim_type', 'emergency_type.name as emergency_type',
                'hospitals.name as hospital_name', 'hospitals.city as hospital_city',
                'emergency_locations.name as ambulance_name', 'emergency_locations.city as ambulance_city', 'emergency_locations.vehicle_type_id as ambulance_type',
                'emergency_locations.longitude as ambulance_longitude', 'emergency_locations.latitude as ambulance_latitude'
            )
            ->where('accidents.id', $id)
            ->get();
        return view('templates/accident',
            [
                'accident' => $accident[0],
            ]
        );
    }

    public function nearestAmbulance(Request $request, $id)
    {
        $accident = Accident::find($id);
        if ($accident->hospital_id && $accident->ambulance_id) {
            $hospitalsLocations = DB::table('hospitals')
                ->where('id', $accident->hospital_id)
                ->get();
            $emergencyLocations = DB::table('emergency_locations')
                ->where('id', $accident->ambulance_id)
                ->get();
        } else {
            $hospitalsLocations = DB::table('hospitals')
                ->join('hospitals_type', 'hospitals_type.id', '=', 'hospitals.type_id')
                ->join('emergency_type', 'emergency_type.type_of_hospitals', '=', 'hospitals_type.id')
                ->select('hospitals.*')
                ->where(
                    [
                        ['hospitals_type.type_of_victim_id', '=', $accident->type_of_victim],
                        ['emergency_type.id', '=', $accident->emergency],
                    ]
                )
                ->groupBy('hospitals.id')
                ->get();
            if (count($hospitalsLocations) == 0) {
                $hospitalsLocations = DB::table('hospitals')
                    ->join('hospitals_type', 'hospitals_type.id', '=', 'hospitals.type_id')
                    ->select('hospitals.*')
                    ->where(
                        [
                            ['hospitals_type.type_of_victim_id', '=', $accident->type_of_victim],
                        ]
                    )
                    ->groupBy('hospitals.id')
                    ->get();
            }

            $emergencyLocations = DB::table('emergency_locations')->get();
        }
        return view('templates/accident/road',
            [   "emergencyLocations" => $emergencyLocations,
                'accident' => $accident,
                'hospitals' => $hospitalsLocations
            ]
        );
    }

    public function sendNotificationToAmbulance($accident) {
        try {
            $emergencyType = DB::table('emergency_type')
                ->where('emergency_type.id', $accident['emergency'])
                ->get()
                ->toArray();
            $emergency_vehicles = DB::table('emergency_vehicles')
                ->whereIn('id', explode(',', $emergencyType[0]->type_of_emergency_vehicle))
                ->get();
            $emergency_vehicles = json_decode(json_encode($emergency_vehicles), true);;
            $content = "Alerta! " . $emergencyType[0]->name . " Este nevoie de una dintre urmatoarele ambulante: " . implode(", ", array_column($emergency_vehicles, 'name'))
                . ", la urmatoarea adresa: " . $accident['location_details'];
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_AUTH_TOKEN");
            $twilio_number = getenv("TWILIO_NUMBER");
            $client = new Client($account_sid, $auth_token);
            $client->messages->create('+400775532063',
                ['from' => $twilio_number, 'body' => $content]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}