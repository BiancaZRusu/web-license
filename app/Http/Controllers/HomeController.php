<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mapper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accidents = DB::table('accidents')
            ->join('type_of_victims', 'type_of_victims.id', '=', 'accidents.type_of_victim')
            ->join('emergency_type', 'emergency_type.id', '=', 'accidents.emergency')
            ->select('accidents.*', 'type_of_victims.type as victim_type', 'emergency_type.name as emergency_type')
            ->get();
        Mapper::map(47.1562327, 27.5169309, ['marker' => false, 'zoom' => 12]);
        foreach ($accidents as $accident) {
            if ($accident->victims_number > 1) {
                $attr = " " . $accident->victims_number . " victime";
            } else {
                $attr = " o victima";
            }
            $accident->type_of_victim = $accident->victim_type;
            $accident->emergency = $accident->emergency_type;
            $marker = $accident->description . "<br>" . $accident->emergency . "<br>" . $attr;
            Mapper::informationWindow($accident->latitude, $accident->longitude, $marker);
        }
        return view('templates/dashboard', ["accidents" => $accidents]);
    }
}
