<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accidents = DB::table('accidents')
            ->select(DB::raw('count(id) as total_accidents'),  DB::raw('DATE(created_at) as created_at'))
            ->groupBy(DB::raw('DATE(created_at)'))
            ->get();
        return view('templates/reports', ["accidents" => $accidents]);
    }
}
